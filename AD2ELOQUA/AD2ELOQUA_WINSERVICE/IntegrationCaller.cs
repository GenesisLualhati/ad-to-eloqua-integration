﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;

namespace AD2ELOQUA_WINSERVICE
{
    public partial class IntegrationCaller : ServiceBase
    {
        private int timeInterval;
        private string webUrl;
        public IntegrationCaller(int _timeInterval, string _webUrl)
        {
            this.timeInterval = _timeInterval;
            this.webUrl = _webUrl;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Timer timer = new Timer();
            timer.Interval = this.timeInterval; // 1 week
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(webUrl);
            request.Method = "get";
            var response = (HttpWebResponse)request.GetResponse();
            Console.Write(response.ToString());
        }

        protected override void OnStop()
        {
        }
    }
}
