﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AD2ELOQUA_WINSERVICE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new IntegrationCaller(int.Parse(ConfigurationManager.AppSettings["TimeIntervalPerMillisec"]), ConfigurationManager.AppSettings["AD2ELOQUAWEB_URL"])
            };
            ServiceBase.Run(ServicesToRun);

        }
    }
}
