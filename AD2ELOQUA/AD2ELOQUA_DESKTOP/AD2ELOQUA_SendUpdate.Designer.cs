﻿namespace AD2ELOQUA_DESKTOP
{
    partial class AD2ELOQUA_SendUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkbxStartDate = new System.Windows.Forms.CheckBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.btnSendUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkbxStartDate
            // 
            this.chkbxStartDate.AutoSize = true;
            this.chkbxStartDate.Location = new System.Drawing.Point(22, 23);
            this.chkbxStartDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkbxStartDate.Name = "chkbxStartDate";
            this.chkbxStartDate.Size = new System.Drawing.Size(109, 24);
            this.chkbxStartDate.TabIndex = 0;
            this.chkbxStartDate.Text = "Start Date";
            this.chkbxStartDate.UseVisualStyleBackColor = true;
            this.chkbxStartDate.CheckedChanged += new System.EventHandler(this.chkbxStartDate_CheckedChanged);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Enabled = false;
            this.dtpStartDate.Location = new System.Drawing.Point(22, 58);
            this.dtpStartDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(298, 26);
            this.dtpStartDate.TabIndex = 1;
            // 
            // btnSendUpdate
            // 
            this.btnSendUpdate.Location = new System.Drawing.Point(376, 23);
            this.btnSendUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSendUpdate.Name = "btnSendUpdate";
            this.btnSendUpdate.Size = new System.Drawing.Size(150, 46);
            this.btnSendUpdate.TabIndex = 2;
            this.btnSendUpdate.Text = "Send Update";
            this.btnSendUpdate.UseVisualStyleBackColor = true;
            this.btnSendUpdate.Click += new System.EventHandler(this.btnSendUpdate_Click);
            // 
            // AD2ELOQUA_SendUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 125);
            this.Controls.Add(this.btnSendUpdate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.chkbxStartDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AD2ELOQUA_SendUpdate";
            this.Text = "Update Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkbxStartDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Button btnSendUpdate;
    }
}