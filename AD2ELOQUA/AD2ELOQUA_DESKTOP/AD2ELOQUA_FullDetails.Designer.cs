﻿namespace AD2ELOQUA_DESKTOP
{
    partial class AD2ELOQUA_FullDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbltxtEmployeeStatus = new System.Windows.Forms.Label();
            this.lbltxtAccountClass = new System.Windows.Forms.Label();
            this.lbltxtDepartment = new System.Windows.Forms.Label();
            this.lbltxtCity = new System.Windows.Forms.Label();
            this.lbltxtCountryCode = new System.Windows.Forms.Label();
            this.lbltxtStateCode = new System.Windows.Forms.Label();
            this.lbltxtBusinessUnit = new System.Windows.Forms.Label();
            this.lbltxtBusinessDivision = new System.Windows.Forms.Label();
            this.lbltxtTitle = new System.Windows.Forms.Label();
            this.lbltxtEmailAddress = new System.Windows.Forms.Label();
            this.lbltxtLastName = new System.Windows.Forms.Label();
            this.lbltxtFirstName = new System.Windows.Forms.Label();
            this.lbltxtDirectSupervisor = new System.Windows.Forms.Label();
            this.lbltxtContactID = new System.Windows.Forms.Label();
            this.lblAccountClass = new System.Windows.Forms.Label();
            this.lblEmployeeStatus = new System.Windows.Forms.Label();
            this.lblDirectSupervisor = new System.Windows.Forms.Label();
            this.lblStateCode = new System.Windows.Forms.Label();
            this.lblCountryCode = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.lblBusinessUnit = new System.Windows.Forms.Label();
            this.lblBusinessDivision = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblContactID = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.lbltxtEmployeeStatus);
            this.groupBox1.Controls.Add(this.lbltxtAccountClass);
            this.groupBox1.Controls.Add(this.lbltxtDepartment);
            this.groupBox1.Controls.Add(this.lbltxtCity);
            this.groupBox1.Controls.Add(this.lbltxtCountryCode);
            this.groupBox1.Controls.Add(this.lbltxtStateCode);
            this.groupBox1.Controls.Add(this.lbltxtBusinessUnit);
            this.groupBox1.Controls.Add(this.lbltxtBusinessDivision);
            this.groupBox1.Controls.Add(this.lbltxtTitle);
            this.groupBox1.Controls.Add(this.lbltxtEmailAddress);
            this.groupBox1.Controls.Add(this.lbltxtLastName);
            this.groupBox1.Controls.Add(this.lbltxtFirstName);
            this.groupBox1.Controls.Add(this.lbltxtDirectSupervisor);
            this.groupBox1.Controls.Add(this.lbltxtContactID);
            this.groupBox1.Controls.Add(this.lblAccountClass);
            this.groupBox1.Controls.Add(this.lblEmployeeStatus);
            this.groupBox1.Controls.Add(this.lblDirectSupervisor);
            this.groupBox1.Controls.Add(this.lblStateCode);
            this.groupBox1.Controls.Add(this.lblCountryCode);
            this.groupBox1.Controls.Add(this.lblCity);
            this.groupBox1.Controls.Add(this.lblDepartment);
            this.groupBox1.Controls.Add(this.lblBusinessUnit);
            this.groupBox1.Controls.Add(this.lblBusinessDivision);
            this.groupBox1.Controls.Add(this.lblTitle);
            this.groupBox1.Controls.Add(this.lblEmailAddress);
            this.groupBox1.Controls.Add(this.lblLastName);
            this.groupBox1.Controls.Add(this.lblFirstName);
            this.groupBox1.Controls.Add(this.lblContactID);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(50, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(975, 350);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee Information";
            // 
            // lbltxtEmployeeStatus
            // 
            this.lbltxtEmployeeStatus.AutoSize = true;
            this.lbltxtEmployeeStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtEmployeeStatus.Location = new System.Drawing.Point(723, 225);
            this.lbltxtEmployeeStatus.Name = "lbltxtEmployeeStatus";
            this.lbltxtEmployeeStatus.Size = new System.Drawing.Size(126, 20);
            this.lbltxtEmployeeStatus.TabIndex = 27;
            this.lbltxtEmployeeStatus.Text = "EmployeeStatus";
            // 
            // lbltxtAccountClass
            // 
            this.lbltxtAccountClass.AutoSize = true;
            this.lbltxtAccountClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtAccountClass.Location = new System.Drawing.Point(723, 260);
            this.lbltxtAccountClass.Name = "lbltxtAccountClass";
            this.lbltxtAccountClass.Size = new System.Drawing.Size(107, 20);
            this.lbltxtAccountClass.TabIndex = 26;
            this.lbltxtAccountClass.Text = "AccountClass";
            // 
            // lbltxtDepartment
            // 
            this.lbltxtDepartment.AutoSize = true;
            this.lbltxtDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtDepartment.Location = new System.Drawing.Point(723, 50);
            this.lbltxtDepartment.Name = "lbltxtDepartment";
            this.lbltxtDepartment.Size = new System.Drawing.Size(94, 20);
            this.lbltxtDepartment.TabIndex = 25;
            this.lbltxtDepartment.Text = "Department";
            // 
            // lbltxtCity
            // 
            this.lbltxtCity.AutoSize = true;
            this.lbltxtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtCity.Location = new System.Drawing.Point(723, 85);
            this.lbltxtCity.Name = "lbltxtCity";
            this.lbltxtCity.Size = new System.Drawing.Size(35, 20);
            this.lbltxtCity.TabIndex = 24;
            this.lbltxtCity.Text = "City";
            // 
            // lbltxtCountryCode
            // 
            this.lbltxtCountryCode.AutoSize = true;
            this.lbltxtCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtCountryCode.Location = new System.Drawing.Point(723, 120);
            this.lbltxtCountryCode.Name = "lbltxtCountryCode";
            this.lbltxtCountryCode.Size = new System.Drawing.Size(102, 20);
            this.lbltxtCountryCode.TabIndex = 23;
            this.lbltxtCountryCode.Text = "CountryCode";
            // 
            // lbltxtStateCode
            // 
            this.lbltxtStateCode.AutoSize = true;
            this.lbltxtStateCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtStateCode.Location = new System.Drawing.Point(723, 155);
            this.lbltxtStateCode.Name = "lbltxtStateCode";
            this.lbltxtStateCode.Size = new System.Drawing.Size(86, 20);
            this.lbltxtStateCode.TabIndex = 22;
            this.lbltxtStateCode.Text = "StateCode";
            // 
            // lbltxtBusinessUnit
            // 
            this.lbltxtBusinessUnit.AutoSize = true;
            this.lbltxtBusinessUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtBusinessUnit.Location = new System.Drawing.Point(240, 260);
            this.lbltxtBusinessUnit.Name = "lbltxtBusinessUnit";
            this.lbltxtBusinessUnit.Size = new System.Drawing.Size(103, 20);
            this.lbltxtBusinessUnit.TabIndex = 21;
            this.lbltxtBusinessUnit.Text = "BusinessUnit";
            // 
            // lbltxtBusinessDivision
            // 
            this.lbltxtBusinessDivision.AutoSize = true;
            this.lbltxtBusinessDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtBusinessDivision.Location = new System.Drawing.Point(240, 225);
            this.lbltxtBusinessDivision.Name = "lbltxtBusinessDivision";
            this.lbltxtBusinessDivision.Size = new System.Drawing.Size(128, 20);
            this.lbltxtBusinessDivision.TabIndex = 20;
            this.lbltxtBusinessDivision.Text = "BusinessDivision";
            // 
            // lbltxtTitle
            // 
            this.lbltxtTitle.AutoSize = true;
            this.lbltxtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtTitle.Location = new System.Drawing.Point(240, 190);
            this.lbltxtTitle.Name = "lbltxtTitle";
            this.lbltxtTitle.Size = new System.Drawing.Size(38, 20);
            this.lbltxtTitle.TabIndex = 19;
            this.lbltxtTitle.Text = "Title";
            // 
            // lbltxtEmailAddress
            // 
            this.lbltxtEmailAddress.AutoSize = true;
            this.lbltxtEmailAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtEmailAddress.Location = new System.Drawing.Point(240, 155);
            this.lbltxtEmailAddress.Name = "lbltxtEmailAddress";
            this.lbltxtEmailAddress.Size = new System.Drawing.Size(107, 20);
            this.lbltxtEmailAddress.TabIndex = 18;
            this.lbltxtEmailAddress.Text = "EmailAddress";
            // 
            // lbltxtLastName
            // 
            this.lbltxtLastName.AutoSize = true;
            this.lbltxtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtLastName.Location = new System.Drawing.Point(240, 120);
            this.lbltxtLastName.Name = "lbltxtLastName";
            this.lbltxtLastName.Size = new System.Drawing.Size(82, 20);
            this.lbltxtLastName.TabIndex = 17;
            this.lbltxtLastName.Text = "LastName";
            // 
            // lbltxtFirstName
            // 
            this.lbltxtFirstName.AutoSize = true;
            this.lbltxtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtFirstName.Location = new System.Drawing.Point(240, 85);
            this.lbltxtFirstName.Name = "lbltxtFirstName";
            this.lbltxtFirstName.Size = new System.Drawing.Size(82, 20);
            this.lbltxtFirstName.TabIndex = 16;
            this.lbltxtFirstName.Text = "FirstName";
            // 
            // lbltxtDirectSupervisor
            // 
            this.lbltxtDirectSupervisor.AutoSize = true;
            this.lbltxtDirectSupervisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtDirectSupervisor.Location = new System.Drawing.Point(723, 190);
            this.lbltxtDirectSupervisor.Name = "lbltxtDirectSupervisor";
            this.lbltxtDirectSupervisor.Size = new System.Drawing.Size(126, 20);
            this.lbltxtDirectSupervisor.TabIndex = 15;
            this.lbltxtDirectSupervisor.Text = "DirectSupervisor";
            // 
            // lbltxtContactID
            // 
            this.lbltxtContactID.AutoSize = true;
            this.lbltxtContactID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltxtContactID.Location = new System.Drawing.Point(240, 50);
            this.lbltxtContactID.Name = "lbltxtContactID";
            this.lbltxtContactID.Size = new System.Drawing.Size(82, 20);
            this.lbltxtContactID.TabIndex = 14;
            this.lbltxtContactID.Text = "ContactID";
            // 
            // lblAccountClass
            // 
            this.lblAccountClass.AutoSize = true;
            this.lblAccountClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccountClass.Location = new System.Drawing.Point(545, 260);
            this.lblAccountClass.Name = "lblAccountClass";
            this.lblAccountClass.Size = new System.Drawing.Size(129, 20);
            this.lblAccountClass.TabIndex = 13;
            this.lblAccountClass.Text = "Account Class:";
            // 
            // lblEmployeeStatus
            // 
            this.lblEmployeeStatus.AutoSize = true;
            this.lblEmployeeStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeStatus.Location = new System.Drawing.Point(545, 225);
            this.lblEmployeeStatus.Name = "lblEmployeeStatus";
            this.lblEmployeeStatus.Size = new System.Drawing.Size(150, 20);
            this.lblEmployeeStatus.TabIndex = 12;
            this.lblEmployeeStatus.Text = "Employee Status:";
            // 
            // lblDirectSupervisor
            // 
            this.lblDirectSupervisor.AutoSize = true;
            this.lblDirectSupervisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirectSupervisor.Location = new System.Drawing.Point(545, 190);
            this.lblDirectSupervisor.Name = "lblDirectSupervisor";
            this.lblDirectSupervisor.Size = new System.Drawing.Size(152, 20);
            this.lblDirectSupervisor.TabIndex = 11;
            this.lblDirectSupervisor.Text = "Direct Supervisor:";
            // 
            // lblStateCode
            // 
            this.lblStateCode.AutoSize = true;
            this.lblStateCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateCode.Location = new System.Drawing.Point(545, 155);
            this.lblStateCode.Name = "lblStateCode";
            this.lblStateCode.Size = new System.Drawing.Size(105, 20);
            this.lblStateCode.TabIndex = 10;
            this.lblStateCode.Text = "State Code:";
            // 
            // lblCountryCode
            // 
            this.lblCountryCode.AutoSize = true;
            this.lblCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryCode.Location = new System.Drawing.Point(545, 120);
            this.lblCountryCode.Name = "lblCountryCode";
            this.lblCountryCode.Size = new System.Drawing.Size(123, 20);
            this.lblCountryCode.TabIndex = 9;
            this.lblCountryCode.Text = "Country Code:";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCity.Location = new System.Drawing.Point(545, 85);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(44, 20);
            this.lblCity.TabIndex = 8;
            this.lblCity.Text = "City:";
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartment.Location = new System.Drawing.Point(545, 50);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(109, 20);
            this.lblDepartment.TabIndex = 7;
            this.lblDepartment.Text = "Department:";
            // 
            // lblBusinessUnit
            // 
            this.lblBusinessUnit.AutoSize = true;
            this.lblBusinessUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessUnit.Location = new System.Drawing.Point(60, 260);
            this.lblBusinessUnit.Name = "lblBusinessUnit";
            this.lblBusinessUnit.Size = new System.Drawing.Size(125, 20);
            this.lblBusinessUnit.TabIndex = 6;
            this.lblBusinessUnit.Text = "Business Unit:";
            // 
            // lblBusinessDivision
            // 
            this.lblBusinessDivision.AutoSize = true;
            this.lblBusinessDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessDivision.Location = new System.Drawing.Point(60, 225);
            this.lblBusinessDivision.Name = "lblBusinessDivision";
            this.lblBusinessDivision.Size = new System.Drawing.Size(154, 20);
            this.lblBusinessDivision.TabIndex = 5;
            this.lblBusinessDivision.Text = "Business Division:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(60, 190);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(48, 20);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Title:";
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.AutoSize = true;
            this.lblEmailAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailAddress.Location = new System.Drawing.Point(60, 155);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(129, 20);
            this.lblEmailAddress.TabIndex = 3;
            this.lblEmailAddress.Text = "Email Address:";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(60, 120);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(100, 20);
            this.lblLastName.TabIndex = 2;
            this.lblLastName.Text = "Last Name:";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.Location = new System.Drawing.Point(60, 85);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(101, 20);
            this.lblFirstName.TabIndex = 1;
            this.lblFirstName.Text = "First Name:";
            // 
            // lblContactID
            // 
            this.lblContactID.AutoSize = true;
            this.lblContactID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContactID.Location = new System.Drawing.Point(60, 50);
            this.lblContactID.Name = "lblContactID";
            this.lblContactID.Size = new System.Drawing.Size(101, 20);
            this.lblContactID.TabIndex = 0;
            this.lblContactID.Text = "Contact ID:";
            // 
            // AD2ELOQUA_FullDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1078, 469);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AD2ELOQUA_FullDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Details";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblAccountClass;
        private System.Windows.Forms.Label lblEmployeeStatus;
        private System.Windows.Forms.Label lblDirectSupervisor;
        private System.Windows.Forms.Label lblStateCode;
        private System.Windows.Forms.Label lblCountryCode;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Label lblBusinessUnit;
        private System.Windows.Forms.Label lblBusinessDivision;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblContactID;
        private System.Windows.Forms.Label lbltxtEmployeeStatus;
        private System.Windows.Forms.Label lbltxtAccountClass;
        private System.Windows.Forms.Label lbltxtDepartment;
        private System.Windows.Forms.Label lbltxtCity;
        private System.Windows.Forms.Label lbltxtCountryCode;
        private System.Windows.Forms.Label lbltxtStateCode;
        private System.Windows.Forms.Label lbltxtBusinessUnit;
        private System.Windows.Forms.Label lbltxtBusinessDivision;
        private System.Windows.Forms.Label lbltxtTitle;
        private System.Windows.Forms.Label lbltxtEmailAddress;
        private System.Windows.Forms.Label lbltxtLastName;
        private System.Windows.Forms.Label lbltxtFirstName;
        private System.Windows.Forms.Label lbltxtDirectSupervisor;
        private System.Windows.Forms.Label lbltxtContactID;
    }
}