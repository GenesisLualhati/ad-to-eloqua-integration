﻿namespace AD2ELOQUA_DESKTOP
{
    partial class AD2ELOQUA_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendUpdateToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.sendAllToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.tabCtrlMain = new System.Windows.Forms.TabControl();
            this.tabEmployeesPage = new System.Windows.Forms.TabPage();
            this.lblCountry = new System.Windows.Forms.Label();
            this.cbxCountry = new System.Windows.Forms.ComboBox();
            this.lblBU = new System.Windows.Forms.Label();
            this.cbxBU = new System.Windows.Forms.ComboBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblDept = new System.Windows.Forms.Label();
            this.lblManager = new System.Windows.Forms.Label();
            this.cbxDept = new System.Windows.Forms.ComboBox();
            this.cbxManager = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.txtbxEmployeeName = new System.Windows.Forms.TextBox();
            this.dgvEmployees = new System.Windows.Forms.DataGridView();
            this.tabRecentUpdtPage = new System.Windows.Forms.TabPage();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.chckbxStartDate = new System.Windows.Forms.CheckBox();
            this.dgvRecentUpdt = new System.Windows.Forms.DataGridView();
            this.lblTotalRows = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            this.tabCtrlMain.SuspendLayout();
            this.tabEmployeesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployees)).BeginInit();
            this.tabRecentUpdtPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecentUpdt)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Enabled = false;
            this.dtpStartDate.Location = new System.Drawing.Point(12, 51);
            this.dtpStartDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(298, 26);
            this.dtpStartDate.TabIndex = 7;
            // 
            // menuStrip
            // 
            this.menuStrip.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem,
            this.sendToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1178, 36);
            this.menuStrip.TabIndex = 11;
            this.menuStrip.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStrip});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(69, 30);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // importToolStrip
            // 
            this.importToolStrip.Name = "importToolStrip";
            this.importToolStrip.Size = new System.Drawing.Size(169, 34);
            this.importToolStrip.Text = "Import";
            this.importToolStrip.Click += new System.EventHandler(this.importToolStrip_Click);
            // 
            // sendToolStripMenuItem
            // 
            this.sendToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendUpdateToolStrip,
            this.sendAllToolStrip});
            this.sendToolStripMenuItem.Name = "sendToolStripMenuItem";
            this.sendToolStripMenuItem.Size = new System.Drawing.Size(68, 30);
            this.sendToolStripMenuItem.Text = "Send";
            // 
            // sendUpdateToolStrip
            // 
            this.sendUpdateToolStrip.Name = "sendUpdateToolStrip";
            this.sendUpdateToolStrip.Size = new System.Drawing.Size(217, 34);
            this.sendUpdateToolStrip.Text = "Send Update";
            this.sendUpdateToolStrip.Click += new System.EventHandler(this.sendUpdateToolStrip_Click);
            // 
            // sendAllToolStrip
            // 
            this.sendAllToolStrip.Name = "sendAllToolStrip";
            this.sendAllToolStrip.Size = new System.Drawing.Size(217, 34);
            this.sendAllToolStrip.Text = "Send All";
            this.sendAllToolStrip.Click += new System.EventHandler(this.sendAllToolStrip_Click);
            // 
            // tabCtrlMain
            // 
            this.tabCtrlMain.Controls.Add(this.tabEmployeesPage);
            this.tabCtrlMain.Controls.Add(this.tabRecentUpdtPage);
            this.tabCtrlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlMain.Location = new System.Drawing.Point(0, 36);
            this.tabCtrlMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabCtrlMain.Name = "tabCtrlMain";
            this.tabCtrlMain.SelectedIndex = 0;
            this.tabCtrlMain.Size = new System.Drawing.Size(1178, 583);
            this.tabCtrlMain.TabIndex = 12;
            this.tabCtrlMain.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabCtrlMain_Selected);
            // 
            // tabEmployeesPage
            // 
            this.tabEmployeesPage.Controls.Add(this.lblCountry);
            this.tabEmployeesPage.Controls.Add(this.cbxCountry);
            this.tabEmployeesPage.Controls.Add(this.lblBU);
            this.tabEmployeesPage.Controls.Add(this.cbxBU);
            this.tabEmployeesPage.Controls.Add(this.btnClear);
            this.tabEmployeesPage.Controls.Add(this.lblDept);
            this.tabEmployeesPage.Controls.Add(this.lblManager);
            this.tabEmployeesPage.Controls.Add(this.cbxDept);
            this.tabEmployeesPage.Controls.Add(this.cbxManager);
            this.tabEmployeesPage.Controls.Add(this.btnSearch);
            this.tabEmployeesPage.Controls.Add(this.lblEmployeeName);
            this.tabEmployeesPage.Controls.Add(this.txtbxEmployeeName);
            this.tabEmployeesPage.Controls.Add(this.dgvEmployees);
            this.tabEmployeesPage.Location = new System.Drawing.Point(4, 29);
            this.tabEmployeesPage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabEmployeesPage.Name = "tabEmployeesPage";
            this.tabEmployeesPage.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabEmployeesPage.Size = new System.Drawing.Size(1170, 550);
            this.tabEmployeesPage.TabIndex = 0;
            this.tabEmployeesPage.Text = "Employees";
            this.tabEmployeesPage.UseVisualStyleBackColor = true;
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(884, 15);
            this.lblCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(64, 20);
            this.lblCountry.TabIndex = 12;
            this.lblCountry.Text = "Country";
            // 
            // cbxCountry
            // 
            this.cbxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCountry.FormattingEnabled = true;
            this.cbxCountry.Location = new System.Drawing.Point(676, 15);
            this.cbxCountry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxCountry.Name = "cbxCountry";
            this.cbxCountry.Size = new System.Drawing.Size(200, 28);
            this.cbxCountry.TabIndex = 11;
            // 
            // lblBU
            // 
            this.lblBU.AutoSize = true;
            this.lblBU.Location = new System.Drawing.Point(561, 55);
            this.lblBU.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBU.Name = "lblBU";
            this.lblBU.Size = new System.Drawing.Size(107, 20);
            this.lblBU.TabIndex = 10;
            this.lblBU.Text = "Business Unit";
            // 
            // cbxBU
            // 
            this.cbxBU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBU.FormattingEnabled = true;
            this.cbxBU.Location = new System.Drawing.Point(353, 55);
            this.cbxBU.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxBU.Name = "cbxBU";
            this.cbxBU.Size = new System.Drawing.Size(200, 28);
            this.cbxBU.TabIndex = 9;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(993, 55);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(160, 32);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.Location = new System.Drawing.Point(561, 15);
            this.lblDept.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(94, 20);
            this.lblDept.TabIndex = 7;
            this.lblDept.Text = "Department";
            // 
            // lblManager
            // 
            this.lblManager.AutoSize = true;
            this.lblManager.Location = new System.Drawing.Point(220, 55);
            this.lblManager.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblManager.Name = "lblManager";
            this.lblManager.Size = new System.Drawing.Size(72, 20);
            this.lblManager.TabIndex = 6;
            this.lblManager.Text = "Manager";
            // 
            // cbxDept
            // 
            this.cbxDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDept.FormattingEnabled = true;
            this.cbxDept.Location = new System.Drawing.Point(353, 15);
            this.cbxDept.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxDept.Name = "cbxDept";
            this.cbxDept.Size = new System.Drawing.Size(200, 28);
            this.cbxDept.TabIndex = 5;
            // 
            // cbxManager
            // 
            this.cbxManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxManager.FormattingEnabled = true;
            this.cbxManager.Location = new System.Drawing.Point(12, 55);
            this.cbxManager.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxManager.Name = "cbxManager";
            this.cbxManager.Size = new System.Drawing.Size(200, 28);
            this.cbxManager.TabIndex = 4;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(993, 15);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(160, 32);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(220, 15);
            this.lblEmployeeName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(125, 20);
            this.lblEmployeeName.TabIndex = 2;
            this.lblEmployeeName.Text = "Employee Name";
            // 
            // txtbxEmployeeName
            // 
            this.txtbxEmployeeName.Location = new System.Drawing.Point(12, 15);
            this.txtbxEmployeeName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbxEmployeeName.Name = "txtbxEmployeeName";
            this.txtbxEmployeeName.Size = new System.Drawing.Size(200, 26);
            this.txtbxEmployeeName.TabIndex = 1;
            // 
            // dgvEmployees
            // 
            this.dgvEmployees.AllowUserToAddRows = false;
            this.dgvEmployees.AllowUserToDeleteRows = false;
            this.dgvEmployees.AllowUserToResizeColumns = false;
            this.dgvEmployees.AllowUserToResizeRows = false;
            this.dgvEmployees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEmployees.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmployees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployees.Location = new System.Drawing.Point(0, 102);
            this.dgvEmployees.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvEmployees.Name = "dgvEmployees";
            this.dgvEmployees.ReadOnly = true;
            this.dgvEmployees.RowHeadersVisible = false;
            this.dgvEmployees.RowHeadersWidth = 62;
            this.dgvEmployees.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmployees.Size = new System.Drawing.Size(1170, 454);
            this.dgvEmployees.TabIndex = 0;
            this.dgvEmployees.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployees_CellDoubleClick);
            this.dgvEmployees.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvEmployees_MouseClick);
            // 
            // tabRecentUpdtPage
            // 
            this.tabRecentUpdtPage.Controls.Add(this.btnRefresh);
            this.tabRecentUpdtPage.Controls.Add(this.chckbxStartDate);
            this.tabRecentUpdtPage.Controls.Add(this.dtpStartDate);
            this.tabRecentUpdtPage.Controls.Add(this.dgvRecentUpdt);
            this.tabRecentUpdtPage.Location = new System.Drawing.Point(4, 29);
            this.tabRecentUpdtPage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRecentUpdtPage.Name = "tabRecentUpdtPage";
            this.tabRecentUpdtPage.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRecentUpdtPage.Size = new System.Drawing.Size(1170, 550);
            this.tabRecentUpdtPage.TabIndex = 1;
            this.tabRecentUpdtPage.Text = "Recent Updates";
            this.tabRecentUpdtPage.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(916, 15);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(160, 32);
            this.btnRefresh.TabIndex = 14;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // chckbxStartDate
            // 
            this.chckbxStartDate.AutoSize = true;
            this.chckbxStartDate.Location = new System.Drawing.Point(12, 15);
            this.chckbxStartDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chckbxStartDate.Name = "chckbxStartDate";
            this.chckbxStartDate.Size = new System.Drawing.Size(109, 24);
            this.chckbxStartDate.TabIndex = 13;
            this.chckbxStartDate.Text = "Start Date";
            this.chckbxStartDate.UseVisualStyleBackColor = true;
            this.chckbxStartDate.CheckedChanged += new System.EventHandler(this.chckbxStartDate_CheckedChanged);
            // 
            // dgvRecentUpdt
            // 
            this.dgvRecentUpdt.AllowUserToAddRows = false;
            this.dgvRecentUpdt.AllowUserToDeleteRows = false;
            this.dgvRecentUpdt.AllowUserToResizeRows = false;
            this.dgvRecentUpdt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRecentUpdt.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRecentUpdt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecentUpdt.Location = new System.Drawing.Point(0, 87);
            this.dgvRecentUpdt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvRecentUpdt.Name = "dgvRecentUpdt";
            this.dgvRecentUpdt.ReadOnly = true;
            this.dgvRecentUpdt.RowHeadersVisible = false;
            this.dgvRecentUpdt.RowHeadersWidth = 62;
            this.dgvRecentUpdt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecentUpdt.Size = new System.Drawing.Size(1093, 463);
            this.dgvRecentUpdt.TabIndex = 0;
            this.dgvRecentUpdt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvRecentUpdt_MouseClick);
            // 
            // lblTotalRows
            // 
            this.lblTotalRows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalRows.AutoSize = true;
            this.lblTotalRows.Location = new System.Drawing.Point(994, 39);
            this.lblTotalRows.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalRows.Name = "lblTotalRows";
            this.lblTotalRows.Size = new System.Drawing.Size(96, 20);
            this.lblTotalRows.TabIndex = 13;
            this.lblTotalRows.Text = "Total Rows: ";
            // 
            // AD2ELOQUA_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1178, 619);
            this.Controls.Add(this.lblTotalRows);
            this.Controls.Add(this.tabCtrlMain);
            this.Controls.Add(this.menuStrip);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AD2ELOQUA_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form";
            this.Load += new System.EventHandler(this.tabCtrlMain_Selected);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.tabCtrlMain.ResumeLayout(false);
            this.tabEmployeesPage.ResumeLayout(false);
            this.tabEmployeesPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployees)).EndInit();
            this.tabRecentUpdtPage.ResumeLayout(false);
            this.tabRecentUpdtPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecentUpdt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStrip;
        private System.Windows.Forms.ToolStripMenuItem sendUpdateToolStrip;
        private System.Windows.Forms.ToolStripMenuItem sendAllToolStrip;
        private System.Windows.Forms.TabControl tabCtrlMain;
        private System.Windows.Forms.TabPage tabEmployeesPage;
        private System.Windows.Forms.TabPage tabRecentUpdtPage;
        private System.Windows.Forms.CheckBox chckbxStartDate;
        private System.Windows.Forms.DataGridView dgvEmployees;
        private System.Windows.Forms.DataGridView dgvRecentUpdt;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.TextBox txtbxEmployeeName;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblDept;
        private System.Windows.Forms.Label lblManager;
        private System.Windows.Forms.ComboBox cbxDept;
        private System.Windows.Forms.ComboBox cbxManager;
        private System.Windows.Forms.Label lblTotalRows;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblBU;
        private System.Windows.Forms.ComboBox cbxBU;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.ComboBox cbxCountry;
    }
}

