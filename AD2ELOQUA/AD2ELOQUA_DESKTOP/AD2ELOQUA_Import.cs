﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AD2ELOQUA_CORE;
using AD2ELOQUA_CORE.DataUtilities;
using AD2ELOQUA_CORE.Models;

namespace AD2ELOQUA_DESKTOP
{
    public partial class AD2ELOQUA_Import : Form
    {
        
        public AD2ELOQUA_Import(List<string> countries)
        {
            InitializeComponent();
            countries.Sort();
            countries.Insert(0, "All");
            cbxCountry.DataSource = countries;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            btnImport.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;

            object cbxItem = cbxCountry.SelectedItem;
            string country;
            if (Convert.ToString(cbxItem) == "All")
            {
                country = "";
            }
            else
            {
                country = Convert.ToString(cbxItem);
            }

            ADRetrieveManager adMgr = new ADRetrieveManager();
            List<ContactViewModel> adList = adMgr.getADContactPerCountry(country, true);

            DBManager dbMgr = new DBManager();
            List<ContactViewModel> dbList = dbMgr.GetContacts();

            AnonymousTypeComparer comparer = new AnonymousTypeComparer();
            var contactDiff = adList.Select(x => new ContactViewModel
                                        {
                                        FirstName = x.FirstName,
                                        LastName = x.LastName,
                                        EmailAddress =    x.EmailAddress,
                                        Title = x.Title,
                                        BusinessDivision = x.BusinessDivision,
                                        BusinessUnit = x.BusinessUnit,
                                        Department = x.Department,
                                        City = x.City,
                                        CountryCode = x.CountryCode,
                                        StateCode = x.StateCode,
                                        DirectSupervisor = x.DirectSupervisor,
                                        EmployeeStatus = x.EmployeeStatus,
                                        AccountClass = x.AccountClass
                                        })
                                        .Except(dbList.Select(y => new ContactViewModel
                                        {
                                        FirstName = y.FirstName,
                                        LastName = y.LastName,
                                        EmailAddress = y.EmailAddress,
                                        Title = y.Title,
                                        BusinessDivision = y.BusinessDivision,
                                        BusinessUnit = y.BusinessUnit,
                                        Department = y.Department,
                                        City = y.City,
                                        CountryCode = y.CountryCode,
                                        StateCode = y.StateCode,
                                        DirectSupervisor = y.DirectSupervisor,
                                        EmployeeStatus = y.EmployeeStatus,
                                        AccountClass = y.AccountClass
                                        }), comparer).ToList();


            List<ContactViewModel> contactToImport = new List<ContactViewModel>();
            foreach (var contact in contactDiff)
            {
                contactToImport.Add(new ContactViewModel()
                {
                    FirstName = contact.FirstName,
                    LastName = contact.LastName,
                    EmailAddress = contact.EmailAddress,
                    Title = contact.Title,
                    DirectSupervisor = contact.DirectSupervisor,
                    BusinessDivision = contact.BusinessDivision,
                    BusinessUnit = contact.BusinessUnit,
                    Department = contact.Department,
                    City = contact.City,
                    CountryCode = contact.CountryCode,
                    StateCode = contact.StateCode,
                    EmployeeStatus = (int)contact.EmployeeStatus,
                    AccountClass = contact.AccountClass
                });
            }

            dbMgr.importContacts(contactToImport);

            List<ContactViewModel> updatedAccounts = new List<ContactViewModel>();
            updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(AD2ELOQUA_CORE.UpdateStatus.New, DateTime.Now));
            updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(AD2ELOQUA_CORE.UpdateStatus.Updated, DateTime.Now));

            string caption = "Import Details";
            string message = "Retrieved in AD: " + contactToImport.Count() + " Updated in DB: " + updatedAccounts.Count();
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            Cursor.Current = Cursors.Default;
            btnImport.Enabled = true;
        }
    }
}
