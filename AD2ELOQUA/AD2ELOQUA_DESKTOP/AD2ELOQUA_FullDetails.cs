﻿using AD2ELOQUA_CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AD2ELOQUA_DESKTOP
{
    public partial class AD2ELOQUA_FullDetails : Form
    {
        public AD2ELOQUA_FullDetails(int ID)
        {
            InitializeComponent();

            DBManager dbm = new DBManager();
            var fd = dbm.getFullDetails(ID);

            lbltxtContactID.Text            = fd[0].ContactID.ToString();
            lbltxtFirstName.Text            = fd[0].FirstName.ToString();
            lbltxtLastName.Text             = fd[0].LastName.ToString();
            lbltxtEmailAddress.Text         = fd[0].EmailAddress.ToString();
            lbltxtTitle.Text                = fd[0].Title.ToString();
            lbltxtBusinessDivision.Text     = fd[0].BusinessDivision.ToString();
            lbltxtBusinessUnit.Text         = fd[0].BusinessUnit.ToString();
            lbltxtDepartment.Text           = fd[0].Department.ToString();
            lbltxtCity.Text                 = fd[0].City.ToString();
            lbltxtCountryCode.Text          = fd[0].CountryCode.ToString();
            lbltxtStateCode.Text            = fd[0].StateCode.ToString();
            lbltxtDirectSupervisor.Text     = fd[0].DirectSupervisor.ToString();
            lbltxtEmployeeStatus.Text       = fd[0].EmployeeStatus == 1? "Active" : "Inactive";
            lbltxtAccountClass.Text         = fd[0].AccountClass.ToString();
        }
    }
}
