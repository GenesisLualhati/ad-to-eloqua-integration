﻿using AD2ELOQUA_CORE;
using AD2ELOQUA_CORE.JsonObjects;
using AD2ELOQUA_CORE.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace AD2ELOQUA_DESKTOP
{
    public partial class AD2ELOQUA_SendUpdate : Form
    {
        public AD2ELOQUA_SendUpdate()
        {
            InitializeComponent();
        }

        DBManager dbMgr = new DBManager();

        private void chkbxStartDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbxStartDate.Checked)
            {
                dtpStartDate.Enabled = true;
            }
            else
            {
                dtpStartDate.Enabled = false;
            }
        }

        private void btnSendUpdate_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DateTime? dateStart = null;

            if (dtpStartDate.Enabled)
            {
                dateStart = Convert.ToDateTime(dtpStartDate.Value);
            }

            List<ContactViewModel> updatedAccounts = new List<ContactViewModel>();
            updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(AD2ELOQUA_CORE.UpdateStatus.New, dateStart));
            updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(AD2ELOQUA_CORE.UpdateStatus.Updated, dateStart));
            int counter = 0;
            foreach (ContactViewModel contact in updatedAccounts)
            {
                sendToEloquaForm(contact);
                counter++;
            }

            string caption = "Update Details";
            string message = "Sent to Eloqua: " + counter;
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            Cursor.Current = Cursors.Default;
        }

        private void sendToEloquaForm(ContactViewModel contact)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string url = ConfigurationManager.AppSettings["EloquaPostingFormURL"];
            if (String.IsNullOrEmpty(url)) return;

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            string json = serializer.Serialize(new EloquaFormItem()
            {
                firstname = contact.FirstName,
                lastName = contact.LastName,
                emailaddress = contact.EmailAddress,
                title = contact.Title,
                department = contact.Department,
                city = contact.City,
                countryCode = contact.CountryCode,
                stateCode = contact.StateCode,
                division = contact.BusinessDivision,
                businessUnit = contact.BusinessUnit,
                supervisor = contact.DirectSupervisor,
                accountClass = contact.AccountClass,
                employeeStatus = contact.EmployeeStatus.ToString()
            });

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                if (!string.IsNullOrEmpty(kvp.Key) && !string.IsNullOrEmpty(kvp.Value))
                {
                    if (sb.Length > 0) sb.Append('&');
                    sb.Append(HttpUtility.UrlEncode(kvp.Key));
                    sb.Append('=');
                    sb.Append(HttpUtility.UrlEncode(kvp.Value));
                }
            }

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(sb.ToString());
            }
            var response = httpWebRequest.GetResponse();

        }
    }
}
