﻿using AD2ELOQUA_CORE;
using AD2ELOQUA_CORE.JsonObjects;
using AD2ELOQUA_CORE.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace AD2ELOQUA_DESKTOP
{
    public partial class AD2ELOQUA_Main : Form
    {
        private List<string> countries;
        private List<string> manager;
        private List<string> department;
        private List<string> businessUnit;
        private List<string> countryCode;
        public AD2ELOQUA_Main()
        {
            InitializeComponent();
            this.MinimumSize = new Size(800, 500);

            countries = getCountry();

            manager = getManagers();
            manager.Insert(0, "All");
            cbxManager.DataSource = manager;

            department = getDepartment();
            department.Insert(0, "All");
            cbxDept.DataSource = department;

            businessUnit = getBusinessUnit();
            businessUnit.Insert(0, "All");
            cbxBU.DataSource = businessUnit;

            countryCode = getCountryCode();
            countryCode.Insert(0, "All");
            cbxCountry.DataSource = countryCode;
        }

        private List<string> getCountry()
        {
            ADRetrieveManager adMgr = new ADRetrieveManager();
            List<string> countries = adMgr.getListOfCountries();
            return countries;
        }

        private List<string> getManagers()
        {
            DBManager dbm = new DBManager();
            List<string> managers = dbm.getManagers();
            return managers;
        }

        private List<string> getDepartment()
        {
            DBManager dbm = new DBManager();
            List<string> department = dbm.getDepartment();
            return department;
        }

        private List<string> getBusinessUnit()
        {
            DBManager dbm = new DBManager();
            List<string> businessUnit = dbm.getBusinessUnit();
            return businessUnit;
        }

        private List<string> getCountryCode()
        {
            DBManager dbm = new DBManager();
            List<string> country = dbm.getCountry();
            return country;
        }

        private string getTotalEmployeeRows()
        {
            string rows = "Total Rows: " + Convert.ToString(dgvEmployees.RowCount);
            return rows;
        }

        private string getTotalRcntUpdtRows()
        {
            string rows = "Total Rows: " + Convert.ToString(dgvRecentUpdt.RowCount);
            return rows;
        }

        private void sendToEloquaForm(ContactViewModel contact)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string url = ConfigurationManager.AppSettings["EloquaPostingFormURL"];
            if (String.IsNullOrEmpty(url)) return;

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            string json = serializer.Serialize(new EloquaFormItem()
            {
                firstname = contact.FirstName,
                lastName = contact.LastName,
                emailaddress = contact.EmailAddress,
                title = contact.Title,
                department = contact.Department,
                city = contact.City,
                countryCode = contact.CountryCode,
                stateCode = contact.StateCode,
                division = contact.BusinessDivision,
                businessUnit = contact.BusinessUnit,
                supervisor = contact.DirectSupervisor,
                accountClass = contact.AccountClass,
                employeeStatus = contact.EmployeeStatus.ToString()
            });

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                if (!string.IsNullOrEmpty(kvp.Key) && !string.IsNullOrEmpty(kvp.Value))
                {
                    if (sb.Length > 0) sb.Append('&');
                    sb.Append(HttpUtility.UrlEncode(kvp.Key));
                    sb.Append('=');
                    sb.Append(HttpUtility.UrlEncode(kvp.Value));
                }
            }

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(sb.ToString());
            }
            var response = httpWebRequest.GetResponse();

        }

        private void importToolStrip_Click(object sender, EventArgs e)
        {
            AD2ELOQUA_Import importForm = new AD2ELOQUA_Import(countries);
            importForm.Show();
        }

        private void sendUpdateToolStrip_Click(object sender, EventArgs e)
        {
            AD2ELOQUA_SendUpdate sendUpdateForm = new AD2ELOQUA_SendUpdate();
            sendUpdateForm.Show();
        }

        private void sendAllToolStrip_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string caption = "Confirm";
            string message = "Are you sure you want to Send All?";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                DBManager dbMgr = new DBManager();
                List<ContactViewModel> updatedAccounts = dbMgr.GetContacts();
                int counter = 1;
                foreach (ContactViewModel contact in updatedAccounts)
                {
                    sendToEloquaForm(contact);
                    counter++;
                }

                caption = "Sent Details";
                message = "Sent to Eloqua: " + counter;
                buttons = MessageBoxButtons.OK;

                result = MessageBox.Show(message, caption, buttons);
            }

            Cursor.Current = Cursors.Default;
        }

        private void chckbxStartDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chckbxStartDate.Checked)
            {
                dtpStartDate.Enabled = true;
            }
            else
            {
                dtpStartDate.Enabled = false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            btnRefresh.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;

            DateTime? dateStart;

            if (dtpStartDate.Enabled)
            {
                dateStart = Convert.ToDateTime(dtpStartDate.Value);
            }
            else
            {
                dateStart = null;
            }

            DBManager dbm = new DBManager();
            DataTable dt = dbm.getRecentUpdates(dateStart);

            dgvRecentUpdt.DataSource = dt;
            lblTotalRows.Text = getTotalRcntUpdtRows();

            Cursor.Current = Cursors.Default;
            btnRefresh.Enabled = true; 
        }

        private void tabCtrlMain_Selected(object sender, EventArgs e)
        {
            if (tabCtrlMain.SelectedTab == tabEmployeesPage)
            {
                Cursor.Current = Cursors.WaitCursor;

                DBManager dbm = new DBManager();
                DataTable dt = dbm.getEmployees(txtbxEmployeeName.Text, Convert.ToString(cbxManager.SelectedItem), Convert.ToString(cbxDept.SelectedItem), Convert.ToString(cbxBU.SelectedItem), Convert.ToString(cbxCountry.SelectedItem));

                dgvEmployees.DataSource = dt;
                lblTotalRows.Text = getTotalEmployeeRows();

                Cursor.Current = Cursors.Default;

            }
            else if (tabCtrlMain.SelectedTab == tabRecentUpdtPage)
            {
                Cursor.Current = Cursors.WaitCursor;

                DateTime? dateStart;

                if (dtpStartDate.Enabled)
                {
                    dateStart = Convert.ToDateTime(dtpStartDate.Value);
                }
                else
                {
                    dateStart = null;
                }

                DBManager dbm = new DBManager();
                DataTable dt = dbm.getRecentUpdates(dateStart);

                dgvRecentUpdt.DataSource = dt;
                lblTotalRows.Text = getTotalRcntUpdtRows();

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;

            DBManager dbm = new DBManager();
            DataTable dt = dbm.getEmployees(txtbxEmployeeName.Text, Convert.ToString(cbxManager.SelectedItem), Convert.ToString(cbxDept.SelectedItem), Convert.ToString(cbxBU.SelectedItem), Convert.ToString(cbxCountry.SelectedItem));

            dgvEmployees.DataSource = dt;
            lblTotalRows.Text = getTotalEmployeeRows();

            Cursor.Current = Cursors.Default;
            btnSearch.Enabled = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (txtbxEmployeeName.Text != "" || cbxManager.SelectedIndex != 0 || cbxDept.SelectedIndex != 0 || cbxBU.SelectedIndex != 0 || cbxCountry.SelectedIndex != 0)
            {
                btnClear.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;

                txtbxEmployeeName.Clear();
                cbxManager.SelectedIndex = 0;
                cbxDept.SelectedIndex = 0;
                cbxBU.SelectedIndex = 0;
                cbxCountry.SelectedIndex = 0;

                DBManager dbm = new DBManager();
                DataTable dt = dbm.getEmployees(txtbxEmployeeName.Text, Convert.ToString(cbxManager.SelectedItem), Convert.ToString(cbxDept.SelectedItem), Convert.ToString(cbxBU.SelectedItem), Convert.ToString(cbxCountry.SelectedItem));

                dgvEmployees.DataSource = dt;
                lblTotalRows.Text = getTotalEmployeeRows();

                Cursor.Current = Cursors.Default;
                btnClear.Enabled = true;
            }
        }

        private void copyEmp_Click(object sender, EventArgs e)
        {
            dgvEmployees.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            Clipboard.SetDataObject(dgvEmployees.GetClipboardContent());
        }

        private void copyRcnt_Click(object sender, EventArgs e)
        {
            dgvRecentUpdt.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            Clipboard.SetDataObject(dgvRecentUpdt.GetClipboardContent());
        }

        private void copyWithHeadersEmp_Click(object sender, EventArgs e)
        {
            dgvEmployees.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            Clipboard.SetDataObject(dgvEmployees.GetClipboardContent());
            dgvEmployees.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
        }

        private void copyWithHeadersRcnt_Click(object sender, EventArgs e)
        {
            dgvRecentUpdt.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            Clipboard.SetDataObject(dgvRecentUpdt.GetClipboardContent());
            dgvRecentUpdt.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
        }

        private void selectAllEmp_Click(object sender, EventArgs e)
        {
            dgvEmployees.SelectAll();
        }

        private void selectAllRcnt_Click(object sender, EventArgs e)
        {
            dgvRecentUpdt.SelectAll();
        }

        private void deleteRcnt_Click(object sender, EventArgs e)
        {
            try
            {
                string confirmCaption = "Confirm";
                string confirmMessage = "Are you sure you want to delete record?";
                MessageBoxButtons confirmButtons = MessageBoxButtons.YesNo;
                DialogResult confirmResult;

                confirmResult = MessageBox.Show(confirmMessage, confirmCaption, confirmButtons);

                if (confirmResult == DialogResult.Yes)
                {
                    string deleteEmrCaption = "Delete Record";
                    string deleteEmrMessage = "Delete also in EmrContacts?";
                    MessageBoxButtons deleteEmrButtons = MessageBoxButtons.YesNoCancel;
                    DialogResult deleteEmrResult;

                    deleteEmrResult = MessageBox.Show(deleteEmrMessage, deleteEmrCaption, deleteEmrButtons);

                    if(deleteEmrResult == DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        DBManager dbm = new DBManager();

                        foreach (DataGridViewRow r in dgvRecentUpdt.SelectedRows)
                        {
                            dbm.deleteRecentUpdates(r.Cells[2].Value.ToString(), true);
                        }

                        btnRefresh_Click(null, null);

                        Cursor.Current = Cursors.Default;
                    }
                    else if (deleteEmrResult == DialogResult.No)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        DBManager dbm = new DBManager();

                        foreach (DataGridViewRow r in dgvRecentUpdt.SelectedRows)
                        {
                            dbm.deleteRecentUpdates(r.Cells[2].Value.ToString(), false);
                        }

                        btnRefresh_Click(null, null);

                        Cursor.Current = Cursors.Default;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Deleting Failed:" + ex.Message.ToString(), "Delete",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvEmployees_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip mnu = new ContextMenuStrip();
                ToolStripMenuItem selectAll = new ToolStripMenuItem("Select All");
                ToolStripMenuItem copy = new ToolStripMenuItem("Copy");
                ToolStripMenuItem copyWithHeaders = new ToolStripMenuItem("Copy with Headers");

                mnu.Items.AddRange(new ToolStripItem[] { selectAll, copy, copyWithHeaders });
                mnu.Show(dgvEmployees, new Point(e.X, e.Y));

                selectAll.Click += new EventHandler(selectAllEmp_Click);
                copy.Click += new EventHandler(copyEmp_Click);
                copyWithHeaders.Click += new EventHandler(copyWithHeadersEmp_Click);
            }
        }

        private void dgvRecentUpdt_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip mnu = new ContextMenuStrip();
                ToolStripMenuItem selectAll = new ToolStripMenuItem("Select All");
                ToolStripMenuItem copy = new ToolStripMenuItem("Copy");
                ToolStripMenuItem copyWithHeaders = new ToolStripMenuItem("Copy with Headers");
                ToolStripMenuItem delete = new ToolStripMenuItem("Delete");

                mnu.Items.AddRange(new ToolStripItem[] { selectAll, copy, copyWithHeaders, delete });
                mnu.Show(dgvRecentUpdt, new Point(e.X, e.Y));

                selectAll.Click += new EventHandler(selectAllRcnt_Click);
                copy.Click += new EventHandler(copyRcnt_Click);
                copyWithHeaders.Click += new EventHandler(copyWithHeadersRcnt_Click);
                delete.Click += new EventHandler(deleteRcnt_Click);  
            }
        }

        private void dgvEmployees_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            int ID = Convert.ToInt16(dgvEmployees.Rows[e.RowIndex].Cells[0].Value.ToString());
            AD2ELOQUA_FullDetails fullDetails = new AD2ELOQUA_FullDetails(ID);
            fullDetails.Show();

            Cursor.Current = Cursors.Default;
        }
    }
}
