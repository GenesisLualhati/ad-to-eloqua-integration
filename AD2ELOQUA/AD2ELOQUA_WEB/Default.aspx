﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AD2ELOQUA_WEB.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>List of Methods </h3>
    <ul>
        <li><asp:HyperLink runat="server" id="link1" Text="Get Countries"></asp:HyperLink></li>
        <li><asp:HyperLink runat="server" id="link2" Text="Import"></asp:HyperLink></li>
        <li><asp:HyperLink runat="server" id="link3" Text="Import And Send Update"></asp:HyperLink></li>
        <li><asp:HyperLink runat="server" id="link4" Text="Send Update"></asp:HyperLink>
            <span>To include date enter "dateStart=MM/DD/YYYY"</span>
        </li>
        <li><asp:HyperLink runat="server" id="link4_1" Text="Send All"></asp:HyperLink></li>
        <li><asp:HyperLink runat="server" id="link5" Text="Get Updated Account Status"></asp:HyperLink>
            <span>To include date enter "dateStart=MM/DD/YYYY"</span>
        </li>
        <li><asp:HyperLink runat="server" id="link6" Text="Get Account Details"></asp:HyperLink>
              <span>Enter Email address "email=example@domain.com"</span>
        </li>
    </ul>
    </div>
    </form>
</body>
</html>
