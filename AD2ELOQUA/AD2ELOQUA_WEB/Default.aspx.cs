﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Net.Http;
using System.Configuration;
using System.Text;
using AD2ELOQUA_CORE;
using AD2ELOQUA_CORE.Models;
using AD2ELOQUA_CORE.JsonObjects;
using Newtonsoft.Json;

namespace AD2ELOQUA_WEB
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populateLinks();

            if (Request.QueryString["action"] != null)
            {
                DBManager dbMgr = new DBManager();
                if (Request.QueryString["action"].Equals("getCountries", StringComparison.CurrentCultureIgnoreCase))
                {
                    ADRetrieveManager adMgr = new ADRetrieveManager();
                    List<string> countries = adMgr.getListOfCountries();
                    var json = JsonConvert.SerializeObject(countries);
                    Response.Write(json);
                }
                else if (Request.QueryString["action"].Equals("import", StringComparison.CurrentCultureIgnoreCase))
                {
                    ADRetrieveManager adMgr = new ADRetrieveManager();
                    List<ContactViewModel> list = adMgr.getADContactPerCountry("", true);
                    dbMgr.importContacts(list);

                    List<ContactViewModel> updatedAccounts = new List<ContactViewModel>();
                    updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(UpdateStatus.New, DateTime.Now));
                    updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(UpdateStatus.Updated, DateTime.Now));

                    Response.Write("Retrieved in AD: " + list.Count() + " Updated in DB: " + updatedAccounts.Count());

                }
                else if (Request.QueryString["action"].Equals("importAndSendUpate", StringComparison.CurrentCultureIgnoreCase))
                {
                    ADRetrieveManager adMgr = new ADRetrieveManager();

                    List<ContactViewModel> list = adMgr.getADContactPerCountry("", true);
                    dbMgr.importContacts(list);

                    List<ContactViewModel> updatedAccounts = new List<ContactViewModel>();
                    updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(UpdateStatus.New, DateTime.Now));
                    updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(UpdateStatus.Updated, DateTime.Now));

                    int counter = 0;
                    foreach (ContactViewModel contact in updatedAccounts)
                    {
                        sendToEloquaForm(contact);
                        counter++;
                    }

                    Response.Write("Retrieved in AD: " + list.Count() + " Updated in DB: " + updatedAccounts.Count() + " Sent to Eloqua: " + counter);
                }
                else if (Request.QueryString["action"].Equals("sendUpdate", StringComparison.CurrentCultureIgnoreCase))
                {
                    DateTime? dateStart = null;
                    if (Request.QueryString["dateStart"] != null)
                    {
                        dateStart = Convert.ToDateTime(Request.QueryString["dateStart"]);
                    }

                    List<ContactViewModel> updatedAccounts = new List<ContactViewModel>();
                    updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(UpdateStatus.New, dateStart));
                    updatedAccounts.AddRange(dbMgr.GetUpdatedContacts(UpdateStatus.Updated, dateStart));
                    int counter = 0;
                    foreach (ContactViewModel contact in updatedAccounts)
                    {
                        sendToEloquaForm(contact);
                        counter++;
                    }

                    Response.Write("Sent to Eloqua: " + counter);
                }
                else if (Request.QueryString["action"].Equals("sendAll", StringComparison.CurrentCultureIgnoreCase))
                {
                    List<ContactViewModel> updatedAccounts = dbMgr.GetContacts();
                    int counter = 1;
                    foreach (ContactViewModel contact in updatedAccounts)
                    {
                        sendToEloquaForm(contact);
                        counter++;
                    }

                    Response.Write("Sent to Eloqua: " + counter);
                }
                else if (Request.QueryString["action"].Equals("getUpdatedAccountStatus", StringComparison.CurrentCultureIgnoreCase))
                {
                    DateTime? dateStart = null;
                    if (Request.QueryString["dateStart"] != null)
                    {
                        dateStart = Convert.ToDateTime(Request.QueryString["dateStart"]);
                    }
                    Response.Write("New Account : " + dbMgr.GetUpdatedContacts(UpdateStatus.New, dateStart).Count() + " ");
                    Response.Write("New Updated : " + dbMgr.GetUpdatedContacts(UpdateStatus.Updated, dateStart).Count());
                }
                else if (Request.QueryString["action"].Equals("getAccountDetails", StringComparison.CurrentCultureIgnoreCase) && Request.QueryString["email"] != null)
                {
                    ADRetrieveManager adMgr = new ADRetrieveManager();
                    List<ContactViewModel> list = adMgr.getADContactPerEmail(Request.QueryString["email"], true);
                    var json = JsonConvert.SerializeObject(list);
                    Response.Write(json);
                }
            }
        }

        private void sendToEloquaForm(ContactViewModel contact)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string url = ConfigurationManager.AppSettings["EloquaPostingFormURL"];
            if (String.IsNullOrEmpty(url)) return;

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ResolveUrl(url));
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            string json = serializer.Serialize(new EloquaFormItem()
            {
                firstname = contact.FirstName,
                lastName = contact.LastName,
                emailaddress = contact.EmailAddress,
                title = contact.Title,
                department = contact.Department,
                city = contact.City,
                countryCode = contact.CountryCode,
                stateCode = contact.StateCode,
                division = contact.BusinessDivision,
                businessUnit = contact.BusinessUnit,
                supervisor = contact.DirectSupervisor,
                accountClass = contact.AccountClass,
                employeeStatus = contact.EmployeeStatus.ToString()
            });

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            StringBuilder sb = new StringBuilder();
            foreach(KeyValuePair<string, string> kvp in dict)
            {
                if (!string.IsNullOrEmpty(kvp.Key) && !string.IsNullOrEmpty(kvp.Value))
                {
                    if (sb.Length > 0) sb.Append('&');
                    sb.Append(HttpUtility.UrlEncode(kvp.Key));
                    sb.Append('=');
                    sb.Append(HttpUtility.UrlEncode(kvp.Value));
                }
            }

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(sb.ToString());
            }
            var response = httpWebRequest.GetResponse();
            
        }

        private void populateLinks()
        {
            string url = HttpContext.Current.Request.Url.ToString();
            if (url.Contains("aspx?"))
                url = url.Split('?')[0];
            link1.NavigateUrl = string.Format("{0}?action={1}", url, "getCountries");
            link2.NavigateUrl = string.Format("{0}?action={1}", url, "import");
            link3.NavigateUrl = string.Format("{0}?action={1}", url, "importAndSendUpate");
            link4.NavigateUrl = string.Format("{0}?action={1}", url, "sendUpdate");
            link4_1.NavigateUrl = string.Format("{0}?action={1}", url, "sendAll");
            link5.NavigateUrl = string.Format("{0}?action={1}", url, "getUpdatedAccountStatus");
            link6.NavigateUrl = string.Format("{0}?action={1}", url, "getAccountDetails");
        }
    }
}