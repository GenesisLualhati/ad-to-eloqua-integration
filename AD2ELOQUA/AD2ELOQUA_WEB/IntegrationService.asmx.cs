﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.DirectoryServices;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Data;
using AD2ELOQUA_CORE;
using AD2ELOQUA_CORE.Models;
using ChoETL;
using Newtonsoft.Json;
using System.Configuration;

namespace AD2ELOQUA_WEB
{
    /// <summary>
    /// Summary description for IntegrationService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class IntegrationService : System.Web.Services.WebService
    {

        [WebMethod]
        public void ExportADList()
        {
            ADRetrieveManager adMgr = new ADRetrieveManager();
            DBManager dbMgr = new DBManager();
            //List<string> countries = adMgr.getListOfCountries();
            List<string> countries = new List<string>() { "Korea", "Philippines" };
            List<ContactViewModel> accounts = new List<ContactViewModel>();
            Dictionary<String, String> countryCount = new Dictionary<string, string>();
            int totalCount = 0;

            string file = Server.MapPath("Test/output.csv");
            List<ContactViewModel> list;
            foreach (string co in countries)
            {
                list = adMgr.getADContactPerCountry(co, true);
                accounts.AddRange(list);
                countryCount.Add(co, list.Count.ToString());
                totalCount = totalCount + list.Count;
            }

            dbMgr.importContacts(accounts);
            
            //using (var w = new ChoCSVWriter<ContactViewModel>(file).WithFirstLineHeader())
            //{
            //    w.Write(accounts);
            //}

            var json = JsonConvert.SerializeObject(countryCount);
            Context.Response.Write(json);
            Context.Response.Write(string.Format("Total Count : " + totalCount));
        }

        [WebMethod]
        public void GetUserControls()
        {
            List<string> uc = new List<string>();
            var de = new DirectoryEntry();
            var ds = new DirectorySearcher(de);
            ds.Filter = string.Format("(&(objectCategory=user){0})", ConfigurationManager.AppSettings["FilterContacts"]);
            ds.SearchScope = SearchScope.Subtree;
            ds.PropertiesToLoad.Add("useraccountcontrol"); //Employee Status 512:enable 514:disable
            ds.PropertiesToLoad.Add("extensionattribute4"); //Employee Type Employee or Contrator
            ds.Sort.Direction = System.DirectoryServices.SortDirection.Ascending;
            ds.Sort.PropertyName = "sAMAccountName";
            ds.SizeLimit = int.MaxValue;
            ds.PageSize = 10000;
            SearchResultCollection results = ds.FindAll();
            foreach (SearchResult result in results)
            {
                if (result.Properties.Contains("useraccountcontrol"))
                {
                    string co = result.Properties["useraccountcontrol"][0].ToString();
                    if (!uc.Contains(co) && !String.IsNullOrEmpty(co.Trim()))
                    {
                        uc.Add(co);
                    }
                }
            }

            var json = JsonConvert.SerializeObject(uc);
            Context.Response.Write(json);
        }
    }
}
