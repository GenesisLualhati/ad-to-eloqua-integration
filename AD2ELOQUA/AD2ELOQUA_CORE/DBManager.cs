﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using AD2ELOQUA_CORE.Models;
using System.Data;

namespace AD2ELOQUA_CORE
{
    public enum UpdateStatus
    {
        New = 0,
        Updated = 1
    }

    public class DBManager
    {
        public List<ContactViewModel> GetContacts()
        {
            List<ContactViewModel> contacts = new List<ContactViewModel>();
            using(AD2ELOQUAEntities entity = new AD2ELOQUAEntities())
            {
                var c = entity.SP_GetAllContacts();
                foreach(GetAllContacts_Result r in c)
                {
                    contacts.Add(new ContactViewModel()
                    {
                        FirstName = r.FirstName,
                        LastName = r.LastName,
                        EmailAddress = r.EmailAddress,
                        Title = r.Title,
                        DirectSupervisor = r.DirectSupervisor,
                        BusinessDivision = r.BusinessDivision,
                        BusinessUnit = r.BusinessUnit,
                        Department = r.Department,
                        City = r.City,
                        CountryCode = r.CountryCode,
                        StateCode = r.StateCode,
                        EmployeeStatus = (int)r.EmployeeStatus,
                        AccountClass = r.AccountClass
                    });
                }
            }

            return contacts;
        }

        public List<ContactViewModel> GetUpdatedContacts(UpdateStatus status, DateTime? date)
        {
            List<ContactViewModel> contacts = new List<ContactViewModel>();
            using (AD2ELOQUAEntities entity = new AD2ELOQUAEntities())
            {
                var c = entity.SP_GetUpdatedContacts((int)status, date);
                foreach (GetAllContacts_Result r in c)
                {
                    contacts.Add(new ContactViewModel()
                    {
                        FirstName = r.FirstName,
                        LastName = r.LastName,
                        EmailAddress = r.EmailAddress,
                        Title = r.Title,
                        DirectSupervisor = r.DirectSupervisor,
                        BusinessDivision = r.BusinessDivision,
                        BusinessUnit = r.BusinessUnit,
                        Department = r.Department,
                        City = r.City,
                        CountryCode = r.CountryCode,
                        StateCode = r.StateCode,
                        EmployeeStatus = (int)r.EmployeeStatus,
                        AccountClass = r.AccountClass
                    });
                }
            }

            return contacts;
        }

        public void importContacts(ContactViewModel toImport)
        {
            using (AD2ELOQUAEntities entity = new AD2ELOQUAEntities())
            {
                    entity.Database.ExecuteSqlCommand("EXEC [dbo].[SP_ImportContact] @FirstName, @LastName, @EmailAddress, @Title, @BusinessDivision, @BusinessUnit, @Department, @City, @CountryCode, @StateCode, @DirectSupervisor, @EmployeeStatus, @AccountClass",
                        new SqlParameter("@FirstName", toImport.FirstName),
                        new SqlParameter("@LastName", toImport.LastName),
                        new SqlParameter("@EmailAddress", toImport.EmailAddress),
                        new SqlParameter("@Title", toImport.Title),
                        new SqlParameter("@BusinessDivision", toImport.BusinessDivision),
                        new SqlParameter("@BusinessUnit", toImport.BusinessUnit),
                        new SqlParameter("@Department", toImport.Department),
                        new SqlParameter("@City", toImport.City),
                        new SqlParameter("@CountryCode", toImport.CountryCode),
                        new SqlParameter("@StateCode", toImport.StateCode),
                        new SqlParameter("@DirectSupervisor", toImport.DirectSupervisor),
                        new SqlParameter("@EmployeeStatus", toImport.EmployeeStatus),
                        new SqlParameter("@AccountClass", toImport.AccountClass)
                        );
            }
        }

        public void importContacts(List<ContactViewModel> toImport)
        {
            using (AD2ELOQUAEntities entity = new AD2ELOQUAEntities())
            {
                foreach (ContactViewModel c in toImport)
                {
                    entity.Database.CommandTimeout = 90000000;
                    entity.Database.ExecuteSqlCommand("EXEC [dbo].[SP_ImportContact] @FirstName, @LastName, @EmailAddress, @Title, @BusinessDivision, @BusinessUnit, @Department, @City, @CountryCode, @StateCode, @DirectSupervisor, @EmployeeStatus, @AccountClass",
                        new SqlParameter("@FirstName", c.FirstName),
                        new SqlParameter("@LastName", c.LastName),
                        new SqlParameter("@EmailAddress", c.EmailAddress),
                        new SqlParameter("@Title", c.Title),
                        new SqlParameter("@BusinessDivision", c.BusinessDivision),
                        new SqlParameter("@BusinessUnit", c.BusinessUnit),
                        new SqlParameter("@Department", c.Department),
                        new SqlParameter("@City", c.City),
                        new SqlParameter("@CountryCode", c.CountryCode),
                        new SqlParameter("@StateCode", c.StateCode),
                        new SqlParameter("@DirectSupervisor", c.DirectSupervisor),
                        new SqlParameter("@EmployeeStatus", c.EmployeeStatus),
                        new SqlParameter("@AccountClass", c.AccountClass)
                        );
                }
            }
        }

        public DataTable getEmployees(string employeeName, string managerName, string departmentName, string businessUnit, string countryCode)
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var query = from record in entities.V_GetEmployees
                        select record;

            if (String.IsNullOrEmpty(employeeName))
            {
                if (Convert.ToString(managerName) == "All")
                {
                    if (Convert.ToString(departmentName) == "All")
                    {
                        if (Convert.ToString(businessUnit) == "All")
                        {
                            if (Convert.ToString(countryCode) == "All")
                            {
                            }
                            else
                            {
                                query = query.Where(x => x.CountryCode == countryCode);
                            }
                        }
                        else if (Convert.ToString(countryCode) == "All")
                        {
                            query = query.Where(x => x.BusinessUnit == businessUnit);
                        }
                        else
                        {
                            query = query.Where(x => x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                        }
                    }
                    else if (Convert.ToString(businessUnit) == "All")
                    {
                        if (Convert.ToString(countryCode) == "All")
                        {
                            query = query.Where(x => x.Department == departmentName);
                        }
                        else
                        {
                            query = query.Where(x => x.Department == departmentName && x.CountryCode == countryCode);
                        }
                    }
                    else if (Convert.ToString(countryCode) == "All")
                    {
                        query = query.Where(x => x.Department == departmentName && x.BusinessUnit == businessUnit);
                    }
                    else
                    {
                        query = query.Where(x => x.Department == departmentName && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                    }
                }
                else if (Convert.ToString(departmentName) == "All")
                {
                    if (Convert.ToString(businessUnit) == "All")
                    {
                        if (Convert.ToString(countryCode) == "All")
                        {
                            query = query.Where(x => x.DirectSupervisor == managerName);
                        }
                        else
                        {
                            query = query.Where(x => x.DirectSupervisor == managerName && x.CountryCode == countryCode);
                        }
                    }
                    else if (Convert.ToString(countryCode) == "All")
                    {
                        query = query.Where(x => x.DirectSupervisor == managerName && x.BusinessUnit == businessUnit);
                    }
                    else
                    {
                        query = query.Where(x => x.DirectSupervisor == managerName && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                    }
                }
                else if (Convert.ToString(businessUnit) == "All")
                {
                    if (Convert.ToString(countryCode) == "All")
                    {
                        query = query.Where(x => x.DirectSupervisor == managerName && x.Department == departmentName);
                    }
                    else
                    {
                        query = query.Where(x => x.DirectSupervisor == managerName && x.Department == departmentName && x.CountryCode == countryCode);
                    }
                }
                else if (Convert.ToString(countryCode) == "All")
                {
                    query = query.Where(x => x.DirectSupervisor == managerName && x.Department == departmentName && x.BusinessUnit == businessUnit);
                }
                else
                {
                    query = query.Where(x => x.DirectSupervisor == managerName && x.Department == departmentName && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                }
            }
            else if (Convert.ToString(managerName) == "All")
            {
                if (Convert.ToString(departmentName) == "All")
                {
                    if (Convert.ToString(businessUnit) == "All")
                    {
                        if (Convert.ToString(countryCode) == "All")
                        {
                            query = query.Where(x => x.EmployeeName.Contains(employeeName));
                        }
                        else
                        {
                            query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.CountryCode == countryCode);
                        }
                    }
                    else if (Convert.ToString(countryCode) == "All")
                    {
                        query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.BusinessUnit == businessUnit);
                    }
                    else
                    {
                        query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                    }
                }
                else if (Convert.ToString(businessUnit) == "All")
                {
                    if (Convert.ToString(countryCode) == "All")
                    {
                        query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.Department == departmentName);
                    }
                    else
                    {
                        query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.Department == departmentName && x.CountryCode == countryCode);
                    }
                }
                else if (Convert.ToString(countryCode) == "All")
                {
                    query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.Department == departmentName && x.BusinessUnit == businessUnit);
                }
                else
                {
                    query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.Department == departmentName && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                }
            }
            else if (Convert.ToString(departmentName) == "All")
            {
                if (Convert.ToString(businessUnit) == "All")
                {
                    if (Convert.ToString(countryCode) == "All")
                    {
                        query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName);
                    }
                    else
                    {
                        query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.CountryCode == countryCode);
                    }
                }
                else if (Convert.ToString(countryCode) == "All")
                {
                    query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.BusinessUnit == businessUnit);
                }
                else
                {
                    query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
                }
            }
            else if (Convert.ToString(businessUnit) == "All")
            {
                if (Convert.ToString(countryCode) == "All")
                {
                    query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.Department == departmentName);
                }
                else
                {
                    query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.Department == departmentName && x.CountryCode == countryCode);
                }
            }
            else if (Convert.ToString(countryCode) == "All")
            {
                query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.Department == departmentName && x.BusinessUnit == businessUnit);
            }
            else
            {
                query = query.Where(x => x.EmployeeName.Contains(employeeName) && x.DirectSupervisor == managerName && x.Department == departmentName && x.BusinessUnit == businessUnit && x.CountryCode == countryCode);
            }

            var result = query.OrderByDescending(x => x.DateUpdatedRaw)
                        .ThenBy(x => x.EmployeeName)
                        .Select(x => new { x.ID
                                        , x.FirstName
                                        , x.LastName
                                        , x.EmailAddress
                                        , x.Department
                                        , x.BusinessUnit
                                        , x.City
                                        , x.CountryCode
                                        , x.StateCode
                                        , x.AccountClass
                                        , x.EmployeeStatus
                                        , x.DateUpdated }).ToList();

            ListToDataTable dt = new ListToDataTable();
            DataTable output = dt.ToDataTable(result);
            return output;
        }

        public DataTable getRecentUpdates(DateTime? dateStart)
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var query = from record in entities.V_GetRecentUpdates
                        select record;

            if (dateStart == null)
            {
                query = query.Where(x => x.DateUpdatedRaw >= x.LatestUpdateDate);
            }
            else
            {
                query = query.Where(x => x.DateUpdatedRaw >= dateStart);
            }

            var result = query.OrderByDescending(x => x.DateUpdatedRaw).ThenBy(x => x.EmployeeName).Select(x => new { x.ID, x.EmployeeName, x.EmailAddress, x.DateUpdated, x.UpdateStatus }).ToList();

            ListToDataTable dt = new ListToDataTable();
            DataTable output = dt.ToDataTable(result);
            return output;
        }

        public List<EmrContacts> getFullDetails(int ID)
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var result = entities.EmrContacts
                                 .Where(x => x.ContactID == ID).ToList();
            return result;
        }
        public List<string> getManagers()
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var manager = entities.EmrContacts.Select(x => x.DirectSupervisor).Distinct().OrderBy(y => y).ToList();
            return manager;
        }

        public List<string> getDepartment()
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var department = entities.EmrContacts.Select(x => x.Department).Distinct().OrderBy(y => y).ToList();
            return department;
        }

        public List<string> getBusinessUnit()
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var businessUnit = entities.EmrContacts.Select(x => x.BusinessUnit).Distinct().OrderBy(y => y).ToList();
            return businessUnit;
        }

        public List<string> getCountry()
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var country = entities.EmrContacts.Select(x => x.CountryCode).Distinct().OrderBy(y => y).ToList();
            return country;
        }

        public void deleteRecentUpdates(string email, bool deleteInEmr)
        {
            AD2ELOQUAEntities entities = new AD2ELOQUAEntities();
            var id = entities.EmrContacts
                             .Where(x => x.EmailAddress == email)
                             .Select(x => x.ContactID).FirstOrDefault().ToString();

            entities.SP_DeleteContactRecord(id, deleteInEmr);
        }
    }
}