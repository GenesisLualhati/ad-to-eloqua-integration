﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD2ELOQUA_CORE.JsonObjects
{
    [Serializable]
    public class EloquaFormItem
    {
        public string firstname { get; set; }
        public string lastName { get; set; }
        public string emailaddress { get; set; }
        public string title { get; set; }
        public string department { get; set; }
        public string city { get; set; }
        public string countryCode { get; set; }
        public string stateCode { get; set; }
        public string supervisor { get; set; }
        public string businessUnit { get; set; }
        public string division { get; set; }
        public string accountClass { get; set; }
        public string employeeStatus { get; set; }
    }
}
