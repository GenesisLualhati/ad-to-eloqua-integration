﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.Configuration;
using AD2ELOQUA_CORE.Models;

namespace AD2ELOQUA_CORE
{
    public class ADRetrieveManager
    {
        public ADRetrieveManager()
        {

        }

        public List<string> getListOfCountries()
        {
            List<string> countries = new List<string>();
            var de = new DirectoryEntry();
            var ds = new DirectorySearcher(de);
            ds.Filter = string.Format("(&(objectCategory=user){0})", ConfigurationManager.AppSettings["FilterCountries"]);
            ds.SearchScope = SearchScope.Subtree;
            ds.PropertiesToLoad.Add("co");
            SearchResultCollection results = ds.FindAll();
            foreach (SearchResult result in results)
            {
                if (result.Properties.Contains("co"))
                {
                    string co = result.Properties["co"][0].ToString();
                    if (!countries.Contains(co) && !String.IsNullOrEmpty(co.Trim()))
                    {
                        countries.Add(co);
                    }
                }
            }

            return countries;
        }

        public List<ContactViewModel> getADContactPerEmail(string email, bool removeDuplicate = false)
        {
            return getContact("(mail=" + email + "*)", false, removeDuplicate);
        }

        public List<ContactViewModel> getADContactPerCountry(string country, bool removeDuplicate = false)
        {
            return getContact("(co=" + country + "*)", true, removeDuplicate);
        }
        private List<ContactViewModel> getContact(string inlinefilter, bool applyDefaultFilters, bool removeDuplicate = false)
        {
            List<ContactViewModel> contacts = new List<ContactViewModel>();
            var de = new DirectoryEntry();
            var ds = new DirectorySearcher(de);
            if (applyDefaultFilters)
                ds.Filter = string.Format("(&(objectCategory=user){0}{1})", ConfigurationManager.AppSettings["FilterContacts"], inlinefilter);
            else
                ds.Filter = string.Format("(&(objectCategory=user){0})", inlinefilter);
            ds.SearchScope = SearchScope.Subtree;
            ds.PropertiesToLoad.Add("givenName");
            ds.PropertiesToLoad.Add("sn");
            ds.PropertiesToLoad.Add("mail");
            ds.PropertiesToLoad.Add("manager");
            ds.PropertiesToLoad.Add("title");
            ds.PropertiesToLoad.Add("division");
            ds.PropertiesToLoad.Add("emrbusinessunit");
            ds.PropertiesToLoad.Add("department");
            ds.PropertiesToLoad.Add("l"); //City
            ds.PropertiesToLoad.Add("c"); //Country Code
            ds.PropertiesToLoad.Add("st"); //State Code
            ds.PropertiesToLoad.Add("useraccountcontrol"); //Employee Status 512:enable 514:disable
            ds.PropertiesToLoad.Add("extensionattribute4"); //Employee Type Employee or Contrator
            ds.PropertiesToLoad.Add("company");
            ds.Sort.Direction = System.DirectoryServices.SortDirection.Ascending;
            ds.Sort.PropertyName = "sAMAccountName";
            ds.SizeLimit = int.MaxValue;
            ds.PageSize = 10000;
            SearchResultCollection results = ds.FindAll();
            foreach (SearchResult result in results)
            {
                ContactViewModel contact = new ContactViewModel();
                foreach(string colName in ds.PropertiesToLoad)
                {
                    if (result.Properties.Contains(colName))
                    {
                        switch (colName)
                        {
                            case "givenName": contact.FirstName = result.Properties[colName][0].ToString(); break;
                            case "sn": contact.LastName = result.Properties[colName][0].ToString(); break;
                            case "mail": contact.EmailAddress = result.Properties[colName][0].ToString(); break;
                            case "title": contact.Title = result.Properties[colName][0].ToString(); break;
                            case "manager": contact.DirectSupervisor = result.Properties[colName][0].ToString(); break;
                            case "division": contact.BusinessDivision= result.Properties[colName][0].ToString(); break;
                            case "emrbusinessunit": contact.BusinessUnit = result.Properties[colName][0].ToString(); break;
                            case "department": contact.Department = result.Properties[colName][0].ToString(); break;
                            case "l": contact.City = result.Properties[colName][0].ToString(); break;
                            case "c": contact.CountryCode = result.Properties[colName][0].ToString(); break;
                            case "st": contact.StateCode = result.Properties[colName][0].ToString(); break;
                            case "useraccountcontrol": contact.AccountControl = result.Properties[colName][0].ToString(); break;
                            case "extensionattribute4": contact.AccountClass = result.Properties[colName][0].ToString(); break;
                            case "company": contact.Company = result.Properties[colName][0].ToString(); break;
                        }
                    }
                }
                if (!contacts.Any(c => c.EmailAddress == contact.EmailAddress))
                    contacts.Add(contact);
            }

            return removeDuplicate ? contacts.Distinct(new ContactComparer()).ToList() : contacts;
            
        }

        private class ContactComparer : IEqualityComparer<ContactViewModel>
        {
            public bool Equals(ContactViewModel x, ContactViewModel y)
            {
                if (Object.ReferenceEquals(x, y)) return true;
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) return false;
                return x.EmailAddress.Equals(y.EmailAddress) && x.FullName.Equals(y.FullName);
            }

            public int GetHashCode(ContactViewModel obj)
            {
                return obj.EmailAddress.GetHashCode();
            }
        }


    }
}
