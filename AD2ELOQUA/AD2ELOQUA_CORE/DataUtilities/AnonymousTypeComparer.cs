﻿using AD2ELOQUA_CORE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD2ELOQUA_CORE.DataUtilities
{
    //This comparer is case insensitive
    public class AnonymousTypeComparer : IEqualityComparer<ContactViewModel>
    {
        public bool Equals(ContactViewModel x, ContactViewModel y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (x == null || y == null) return false;
            if (x.GetType() != y.GetType()) return false;

            var properties = x.GetType().GetProperties();
            foreach (var property in properties)
            {
                var valueX = property.GetValue(x);
                var valueY = property.GetValue(y);

                if (!string.Equals(valueX?.ToString(), valueY?.ToString(), StringComparison.OrdinalIgnoreCase))
                    return false;
            }

            return true;
        }

        public int GetHashCode(object obj)
        {
            unchecked
            {
                var properties = obj.GetType().GetProperties();
                int hash = 17;

                foreach (var property in properties)
                {
                    var value = property.GetValue(obj);
                    if (value != null)
                        hash = hash * 23 + value.GetHashCode();
                }

                return hash;
            }
        }

        int IEqualityComparer<ContactViewModel>.GetHashCode(ContactViewModel obj)
        {
            int hcode = obj.EmployeeStatus;
            return  hcode.GetHashCode();
        }
    }
}
