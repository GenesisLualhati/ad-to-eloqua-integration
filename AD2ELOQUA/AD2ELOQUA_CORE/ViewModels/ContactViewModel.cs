﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD2ELOQUA_CORE.Models
{

    public class ContactViewModel
    {
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Title { get; set; }
        public string Department { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string BusinessDivision { get; set; }
        public string BusinessUnit { get; set; } 
        private string _directSupervisor;
        public string DirectSupervisor
        {
            get
            {
                try
                {
                    if (!String.IsNullOrEmpty(_directSupervisor))
                    {
                        string text = _directSupervisor.Substring(0, _directSupervisor.IndexOf(",OU="));
                        text = text.Replace("CN=", "");
                        text = text.Replace(@"\", "");

                        return text;
                    }
                    else
                        return String.Empty;
                }
                catch(Exception)
                {
                    return _directSupervisor;
                }
            }
            set
            {
                _directSupervisor = value;
            }
        }
        public string AccountControl { get; set; }
        public string Company { get; set; }
        public int EmployeeStatus
        {
            get
            {
                return AccountControl.Equals("514") ? 0 : 1;
            }
            set
            {
                AccountControl = value == 1? "512" : "514";
            }
        }
        public string AccountClass { get; set; }

        public ContactViewModel()
        {
            FullName = String.Empty;
            FirstName = String.Empty;
            LastName = String.Empty;
            EmailAddress = String.Empty;
            Title = String.Empty;
            Department = String.Empty;
            City = String.Empty;
            CountryCode = String.Empty;
            StateCode = String.Empty;
            BusinessUnit = String.Empty;
            BusinessDivision = String.Empty;
            AccountControl = "512";
            AccountClass = String.Empty;
            Company = String.Empty;
        }

    }
}
